﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace FizzBuzz
{
    class Program
    {
        /// <summary>
        /// Test for Pyxis interview
        /// </summary>
        public static void Main()
        {
            Configuration oConfiguration = Configuration.Load();
            Writer oWriter = new Writer();

            foreach (int i in To(oConfiguration.Begin, oConfiguration.End))
            {
                if (TestMultiple(i, oConfiguration.I3)
                || TestMultiple(i, oConfiguration.I5))
                {
                    if (TestMultiple(i, oConfiguration.I3))
                    {
                        oWriter.Write(oConfiguration.S3);
                    }
                    if (TestMultiple(i, oConfiguration.I5))
                    {
                        oWriter.Write(oConfiguration.S5);
                    }
                }
                else
                {
                    oWriter.Write(i);
                }
                oWriter.WriteLine();
            }

            Console.ReadKey();
        }

        private static IEnumerable<int> To(int iBegin, int iEnd)
        {
            for (int i=iBegin;i<=iEnd; i++)
            {
                yield return i;
            }
        }

        private static bool TestMultiple(int iInput, int iMultiple)
        {
            return iInput % iMultiple == 0;
        }
    }
}
