﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    public class Writer
    {
        public void Write(int i)
        {
            Write(i.ToString());
        }

        public void Write(string strMessage)
        {
            Console.Write(strMessage);
        }

        public void WriteLine()
        {
            Console.WriteLine();
        }

        public void WriteLine(string strMessage)
        {
            Console.WriteLine(strMessage);
        }
    }
}
