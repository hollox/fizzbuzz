﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    public class Configuration
    {
        public int Begin { get; set; }
        public int End { get; set; }

        public int I3 { get; set; }
        public int I5 { get; set; }

        public string S3 { get; set; }
        public string S5 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iMinimum"></param>
        /// <param name="iMaximum"></param>
        /// <param name="I3"></param>
        /// <param name="I5"></param>
        /// <param name="S3"></param>
        /// <param name="S5"></param>
        public Configuration(int iMinimum, int iMaximum, int I3, int I5, string S3, string S5)
        {
            Begin = iMinimum;
            End = iMaximum;

            this.I3 = I3;
            this.I5 = I5;

            this.S3 = S3;
            this.S5 = S5;
        }

        /// <summary>
        /// Load basic configuration
        /// </summary>
        public static Configuration Load()
        {
            return new Configuration(1, 100, 3, 5, "FIZZ", "BUZZ");
        }
    }
}
